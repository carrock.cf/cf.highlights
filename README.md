
## INTRO
This project highlights various events/words from Carrion Fields (CF) mud.
(For more info, see: http://carrionfields.net/)

If you use a mud client that is compatible with this project, you can load up 
the files for your clients and get all of these highlights for free. 

Currently, only tintin++ is supported and tested.
CMUD _might_ work (more on this below). 

...

This project was based on Dwoggurd's CMUD highlighter, which had many, many cool highlights.
These mostly dealt with various event-messages (e.g. skill success/failure, you being 
targeted, cabal powers, etc). 

In addition to those highlights, Krunk had grepped through his logs to provide me with 
the 'affects' list containing several hundred affects, over the course of his chars.
This covers skills, spells, communes, songs and powers that may affect you, for better
or for worse. 

This project wouldn't be possible without the above two, so both Dwoggurd and Krunk
have my heart-felt thanks for making this happen.

Finally, I maintained a list of 'exploration keywords' for a while, so I decided 
to expand on my work and formalize it a bit. This is a list of words that are 
both highlighted and underlined and might come useful during exploration. 
These are words such as 'chest', 'bookshelf', etc. which appear in room 
descriptions. If you see them, you might want to pay close attention... 



## ORGANIZATION
The project is originally organized in three folders:

* ~/cmud_files/
  Contains only Dwoggurd's original highlighter.
  I parsed this file out and created raw messages that CF spits out. 
  I then placed these raw messages into 'raw_input' file.
  
  Note: 
  Dwoggurd also still (as of end of 2018) has his site up, which explains his script:
  https://sites.google.com/site/dwoggurdsden/dwoggurd-s-den/carrion-fields

  I do not use CMUD, so I don't know if above script works. 
  It's provided as-is. If it doesn't work and you manage to fix it,
  please send me a patch so that I can incorporate it into this project. 


* ~/raw_input/
  This folder contains messages that CF generates, which I extracted from 
  Dwoggurd's script (see above).

  In addition to this, Krunk has generously analyzed a bunch of his logs and 
  provided me with the list of all known affects. 

  Finally, I generated a list of exploration keywords (e.g. 'tome', 'chest',
  'armoire', etc.) that you can use to highlight the room descriptions, which
  will -- hopefully -- help you discover some of the mamy, many secrets that 
  CF holds. 

  All files in this folder end with .raw extension, by design. 

  The list of raw messages is _very_ incomplete. We are missing a bunch of 
  messages for specific classes and events. Please contribute generously 
  when you see an expression missing. 

  For every raw input addition, I will create a corresponding highlight 
  in the tintin section. 


* ~/tintin_files/
  This folder contains all the files from "raw_input", which have been coded to 
  highlight in tintin++ client. 

  All files in this folder end with .tintin extension, by design.


Each of the above folders may contain a brief README.md file, which might explain
some of the not-so-obvious items related to specific implementation.



### TO DO
I mapped the highlighter for tintin++ mud client.
I provided Dwoggurd's CMUD highlighter, as-is.

It would be great if:

* Someone mapped the highlighter for other clients (e.g. ZMUD, Pueblo, MushClient, what-have-you).
  In order to do this, just add another folder to the project and either:
  a) change tintin++ syntax to your client's syntax   OR
  b) use raw expressions to write your client's syntax from scratch.
* Someone supplied more "raw_input" files for various classes that we are missing.
* Someone added more "exploration_keywords.raw".
* Someone pointed out any bugs.


