## GENERAL NOTES
The files in this directory represent finished highlights that you can plug into your
tintin++ config. These files were created by translating the files from ~/raw\_input/ 
folder into the tintin++ syntax.

Therefore, most files in this folder have a direct equivalent in the ~/raw\_input/ folder.

E.g. "affects.raw" -> "affects.tintin".

Only a few files are tintin++ specific:

**colors.tintin**

This file contains colour codes that I used to highlight messages in different colors.
You can open any other file (e.g. 'affects.tintin') and see that when I used
$NegativeAffectColor there, it comes from NegativeAffectColor defined in colors.tintin.


**all\_config\_files.tintin**

This is the file that reads all the other highlight files into your buffer.
Typically, you add this file to your existing script and it automatically loads
up all the other highlights.

More on this in the "USAGE" section below.


**main\_file.tintin**

If you have no existing char file, then you can use 'main\_file.tintin' to 
start to create your character file. 



## USAGE
In this example, we assume that your existing aliases and setup for your
char are saved in a file named "my\_Char" somewhere in the system.

In order to use the highlighter, you should:

0) First and foremost, create a backup of your file. You can call it 
"my\_Char.backup".

1) Copy your "my\_Char" file into the ~/tintin\_files/ folder.
Open your char file and add this one line at the bottom of it:

```#read all_config_files.tintin;```

Save and exit your char file.

2) Start up the tintin++ client with your char file:

```tt++ my_Char```

You should see over 1,000 highlights loaded.
However, as soon as you quit, these definitions will be gone.
Clearly, we must make them permanent. 

3) Make changes permanent.
In order to do this, type:

```#write my_Char  <ENTER>```

```#zap <ENTER>```

Above two actions will write your current setup into your "my\_Char" file and 
then quit (zap) your session. You end up with a massively increased "my\_Char" file
which contains all the highlights from this project!
From this moment on, any time you start up your tintin++ sessions with this "my\_Char" 
file, you should have these highlights effective. 

Note that above "\#write" command overwrites your existing "my\_Char" file, so 
if you want to save your backup version before you overwrite it, please do so 
ahead of time! 

In other words, you really, really should do step 0) first, in case something goes 
wrong. 



## COMPLICATIONS
If you follow all the steps in the "USAGE" section above, you will end up with 
your "my\_Char" file that has all of your old settings + these new highlights.

But what happens with the passage of time?

Case 1: You add some new custom alias and save it to your "my\_Char" file.
In this case, you are good to go. You can make your own custom notifications as much 
as you would like. The key point is that the cf.highlights remain unchanged.

Case 2: There is a newer version of cf.highlights project and your "my\_Char" file
contains old/wrong highlights.

Well, in this case, you need to generate your file from the beginning. 
However, this might be hard to do. For example, you would think that deleting 
all the \#HIGHLIGHT commands might be enough... but what if you have some of your
own highligts defined? And what about \#variables? You will definitely have 
your own vars defined, and you don't want to nit-pick through 1,000+ lines and 
decide what should be kept and what should be deleted.

There are two solutions for this problem.

In either case, please make sure that you have created a backup of your "my\_Char" 
file (step 0) above.

### First approach
Just copy your char file to the ~/tintin_files folder.
Add one line at the bottom:
#READ all_config_files.tintin

Start up the tintin from this folder with your file active:

```tt++ "my\_Char"```

It will re-read all the settings from this project, but it will _hopefully_ not
touch your private settings. Type:

```#write my\_Char ; <ENTER>```
```#zap <ENTER>```

Finally, copy "my\_Char" file back to your tintin++ folder and start using it.

This should result in a quick and dirty way to update your file with new highlights from this project.

### Second approach
Try this only if the quick-and-dirty approach from above fails. There's more work involved here. 

Use the "my\_Char.backup" file that we created as a backup (in step 0) in the "USAGE" section. 

I would personally:
* First, fetch the changes from cf.highlights project. 
* Take the copy of your "my\_Char.backup" file (leave the original as backup).
* Generate the new character file by repeating steps 1-3 of the "USAGE" section.
* Run the 'diff' command between this new file and your currently used "my\_Char" file.
* Decide, based on the 'diff' output, what is added by you and what should be discarded.

The biggest reason for this complication is that tintin++ doesn't just allow you to say
\#read all\_config\_files.tintin

... at the end of your script.
It actually reads *and interprets* all the commands from these config files and expands them.
This is why you end up with over a thousand \#highlight statements. 



